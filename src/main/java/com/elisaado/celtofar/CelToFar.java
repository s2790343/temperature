package com.elisaado.celtofar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.server.ServerCloneException;

public class CelToFar extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public void init() throws ServletException {
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "huts";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"style.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Celcius: " +
                request.getParameter("cel") + "\n" +
                "  <P>Fahrenheit: " +
                (convert(request.getParameter("cel"))) +
                "</BODY></HTML>");
    }

    public String convert(String celcius) {
        double parsed = 0;
        try {
            parsed = Double.parseDouble(celcius);
        } catch (NumberFormatException ignored) {
            return "NO BAD!!!";
        }
        
        return Double.toString(parsed*1.8 + 32);
    }

}
